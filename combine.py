from PIL import *
import numpy as np

def addH(im1, im2):
    neIm=[]
    for y in range(len(np.array(im1))):
        neIm.append(np.array(im1).tolist()[y]+np.array(im2).tolist()[y])
    return Image.fromarray(np.array(neIm).astype(np.uint8))
    
def addV(im1, im2):
    return Image.fromarray(np.array((np.array(im1).tolist()+np.array(im2).tolist())).astype(np.uint8))
