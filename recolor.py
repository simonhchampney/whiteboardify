import numpy as np
from PIL import Image

def decolor(image, palette): # Converts the image to specific set of colors
    for x in range(image.width):
        for y in range(image.height):
            image.putpixel((x,y),closest(palette, image.getpixel((x,y))))
    return image

def closest(palette,color): # Gets the closest color out of a palette of colors
    colors = np.array(palette)
    color = np.array(color)
    distances = np.sqrt(np.sum((colors-color)**2,axis=1))
    index_of_smallest = np.where(distances==np.amin(distances))
    smallest_distance = colors[index_of_smallest]
    return tuple(smallest_distance[0])

def hiloDecolor(img):
    imgar = np.array(img)
    avg = imgar.mean(axis=0).mean()
    for y in range(img.height):
        for x in range(img.width):
            img.putpixel((x,y), 0 if (img.getpixel((x,y))<avg) else 255)
    return img
