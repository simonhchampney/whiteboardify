from PIL import Image, ImageFilter
from recolor import hiloDecolor
from combine import addH, addV

def split(img):
    return [img.crop((0, 0, img.width/2, img.height/2)),
            img.crop((img.width/2, 0, img.width, img.height/2)),
            img.crop((0, img.height/2, img.width/2, img.height)),
            img.crop((img.width/2, img.height/2, img.width, img.height))]

def splitImg(img):
    hilo = img.getextrema()
    if((hilo[1]-hilo[0])<124 or img.width<=1 or img.height<=1):
        return img
    else:
        imgs = []
        for imgc in split(img):
            imgc=splitImg(imgc)
            imgs.append(hiloDecolor(imgc))
        return addV(addH(imgs[0], imgs[1]), addH(imgs[2], imgs[3]))